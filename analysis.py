import ROOT

def passRefCuts(ev):
  muHLT = "HLT_IsoMu24"
  #muIsoCut = 0.2
  muPtCut = 27
  if not( getattr(ev, muHLT, False)): return False
  if ev.nMuon == 0: return False
  for iMu in range(ev.nMuon):
    if (ev.Muon_pt[iMu] >= muPtCut) and ev.Muon_tightId[iMu]:
      return True
  return False


def getJetVars(ev):
  # Apply ID first
  jetPtCut  = 30
  jetEtaCut = 5.0
  jetIDCutsBarrel = { "chEmEF": [-1, 0.99], "chHEF": [0.2, 2], "neEmEF": [-1, 0.99], "neHEF": [-1, 0.9] }
  iGoodJets = []
  for iJ in range(ev.nJet):
    if ev.Jet_pt[iJ] < jetPtCut: continue
    if abs(ev.Jet_eta[iJ]) > jetEtaCut: continue
    if abs(ev.Jet_eta[iJ]) < 2.4 and (ev.Jet_chHEF[iJ] < 0.3): 
      continue
    #for var in jetIDCutsBarrel:
    #  val =  getattr(ev, "Jet_%s"%var)[iJ] 
    #  if not(val >= jetIDCutsBarrel[var][0]) or not(val <= jetIDCutsBarrel[var][1]):
    #    continue      
    if (ev.Jet_jetId[iJ] >= 2):
      iGoodJets.append(iJ)

  # Without two jets we can't build a pair
  if len(iGoodJets) < 2: return  {"detajj": -1, "mjj": -1, "jet1pt": -1, "jet2pt":-1, "ij1":-1, "ij2":-1, "maxjetpt":-1}

  # Then build possible jet pairs and take the one with highest mjj
  jet1p4, jet2p4 = ROOT.TLorentzVector(), ROOT.TLorentzVector()
  deta, mjj, jet1pt, jet2pt, ij1, ij2, maxjetpt = -1, -1, -1, -1, -1, -1, -1
  for iJ1 in iGoodJets:
    if ev.Jet_pt[iJ1] >= maxjetpt: maxjetpt = ev.Jet_pt[iJ1]
    jet1p4.SetPtEtaPhiM(ev.Jet_pt[iJ1], ev.Jet_eta[iJ1], ev.Jet_phi[iJ1], ev.Jet_mass[iJ1])
    for iJ2 in iGoodJets:
      if iJ2 <= iJ1: continue
      jet2p4.SetPtEtaPhiM(ev.Jet_pt[iJ2], ev.Jet_eta[iJ2], ev.Jet_phi[iJ2], ev.Jet_mass[iJ2])

      # Compute variables
      detaNow = abs(jet1p4.Eta() - jet2p4.Eta())
      mjjNow  = (jet1p4+jet2p4).M()
      jet1ptNow, jet2ptNow = ev.Jet_pt[iJ1], ev.Jet_pt[iJ2]

      # Check they pass our offline cuts
      if mjjNow >= mjj:
        #print("replace", jet1pt, jet2pt, jet1ptNow, jet2ptNow)
        deta = detaNow
        mjj = mjjNow
        jet1pt = jet1ptNow
        jet2pt = jet2ptNow
        ij1 = iJ1
        ij2 = iJ2
        #return {"detajj": deta, "mjj": mjj, "jet1pt": jet1pt, "jet2pt":jet2pt}   
  return {"detajj": deta, "mjj": mjj, "jet1pt": jet1pt, "jet2pt":jet2pt, "ij1":ij1, "ij2": ij2, "maxjetpt":maxjetpt} 
