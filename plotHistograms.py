import ROOT
import os
import sys

#####################
### Configuration ###
#####################

histBins = {
  "jet1pt": [25, 0, 250],
  "jet2pt": [35, 0, 350],
  "mjj"   : [25, 0, 2500],
  "detajj": [40, 0, 8]
}

extras = {
  "eta": [40, -5, 5],
  "phi": [40, -3.14, 3.14],
  "chEmEF": [40, 0, 1],
  "chHEF": [40, 0, 1],
  "neEmEF": [40, 0, 1],
  "neHEF": [40, 0, 1],
}

### Plotting ###

inF = ROOT.TFile(sys.argv[1], "READ")

for var in histBins:
  teff = ROOT.TEfficiency(inF.Get(var +"_num"), inF.Get(var+"_den")) 
  c = ROOT.TCanvas(var + "C", var + "C", 800, 600)
  teff.SetTitle(";%s;%s"%(var,"Efficiency"))
  teff.Draw("AP")
  c.Update()
  gr = teff.GetPaintedGraph()
  gr.SetMinimum(0)
  gr.SetMaximum(1.4)
  gr.GetXaxis().SetLimits(histBins[var][1], histBins[var][2])
  c.SaveAs(sys.argv[2] + "/%s.pdf"%(var))
  c.SaveAs(sys.argv[2] + "/%s.png"%(var))

for var in extras:
 for jet in ["ij1", "ij2"]:
  teff = ROOT.TEfficiency(inF.Get(var +jet+"_num"), inF.Get(var+jet+"_den"))
  c = ROOT.TCanvas(var + "C", var + "C", 800, 600)
  teff.SetTitle(";%s;%s"%(var,"Efficiency"))
  teff.Draw("AP")
  c.Update()
  gr = teff.GetPaintedGraph()
  gr.SetMinimum(0)
  gr.SetMaximum(1.4)
  gr.GetXaxis().SetLimits(extras[var][1], extras[var][2])
  c.SaveAs(sys.argv[2] + "/%s%s.pdf"%(var, jet))
  c.SaveAs(sys.argv[2] + "/%s%s.png"%(var, jet))

inF.Close() 
