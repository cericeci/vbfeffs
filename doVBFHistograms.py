import ROOT
import os, sys
from analysis import passRefCuts, getJetVars

#####################
### Configuration ###
#####################

inputFiles   = sys.argv[2:] #[ "/eos/cms/store/user/cericeci/VBFTrigger/nano/promC01v4fb/" + f for f in os.listdir("/eos/cms/store/user/cericeci/VBFTrigger/nano/promC01v4fb/")][:1000] # ["/eos/cms/store/user/cericeci/VBFTrigger/code/../nano/promC01v4fb/nanoaod_0001.root"]
outputHistos = sys.argv[1] #"/eos/user/c/cericeci/www/VBF/"
#if not(os.path.isdir(outputHistos)): os.system("mkdir %s"%(outputHistos))

# HLT Path(s) to measure #
hlt = ["HLT_VBF_DiPFJet105_40_Mjj1000_Detajj3p5", "HLT_VBF_DiPFJet105_40_Mjj1000_Detajj3p5_TriplePFJet"] 

# Offline cuts #
offlineCuts = {
  "jet1pt": 130,
  "jet2pt":  60,
  "mjj":   1300,
  "detajj": 3.7,
}

# Histogram configuration #

histBins = {
  "jet1pt": [25, 0, 250],
  "jet2pt": [35, 0, 350],
  "mjj"   : [25, 0, 2500], 
  "detajj": [40, 0, 8]
}

extras = {
  "eta": [40, -5, 5],
  "phi": [40, -3.14, 3.14],
  "chEmEF": [40, 0, 1],
  "chHEF": [40, 0, 1],
  "neEmEF": [40, 0, 1],
  "neHEF": [40, 0, 1],
}

# Create the actual histograms for saving #
histos = {}

for var in histBins:
  histos[var + "_num"] = ROOT.TH1F(var + "_num", var+"_num", histBins[var][0], histBins[var][1], histBins[var][2])
  histos[var + "_den"] = ROOT.TH1F(var + "_den", var+"_den", histBins[var][0], histBins[var][1], histBins[var][2])

for var in extras:
  for jet in ["ij1", "ij2"]:
    histos[var +jet+ "_num"] = ROOT.TH1F(var + jet+ "_num", var+jet+ "_num", extras[var][0], extras[var][1], extras[var][2])
    histos[var +jet+ "_den"] = ROOT.TH1F(var + jet+ "_den", var+jet+ "_den", extras[var][0], extras[var][1], extras[var][2])


# Now run the thing #
iF = 0
nF = len(inputFiles)
# Counters
for iFile in inputFiles:
  iF += 1
  print("Starting file %i/%i"%(iF, nF))
  tf = ROOT.TFile(iFile, "READ")
  events = tf.Get("Events")
  iEv = 0
  nEv = events.GetEntries()
  for ev in events:
    #if(iEv%1000 == 0): print("%i/%i events in file done"%(iEv, nEv))
    iEv += 1
    if not(passRefCuts(ev)): continue
    # Get all of the variables
    jetVars = getJetVars(ev)
    # Check if we passed denominator
    if jetVars["mjj"] < 0: continue
    passHLT = False
    # Check if we pass numerator
    for hltpath in hlt:
      if getattr(ev, hltpath, False): 
        passHLT = True


    passjet1Cut = (jetVars["jet1pt"] >= offlineCuts["jet1pt"]) or (jetVars["maxjetpt"] >= offlineCuts["jet1pt"])
    passjet2Cut = jetVars["jet2pt"] >= offlineCuts["jet2pt"]
    passdetajjCut = jetVars["detajj"] >= offlineCuts["detajj"]
    passmjjCut  = jetVars["mjj"] >= offlineCuts["mjj"]

    # Then save denominator and numerator
    for var in histBins:
      passDen = False
      if var == "jet1pt": passDen = passjet2Cut and passdetajjCut and passmjjCut
      if var == "jet2pt": passDen = passjet1Cut and passdetajjCut and passmjjCut
      if var == "detajj": passDen = passjet1Cut and passjet2Cut and passmjjCut
      if var == "mjj":    passDen = passjet1Cut and passjet2Cut and passdetajjCut
      if passDen:
        histos[var + "_den"].Fill(jetVars[var])
        if passHLT:
          histos[var + "_num"].Fill(jetVars[var])
    passDen = passjet1Cut and passjet2Cut and passdetajjCut and passmjjCut
    if not(passDen): continue
    for var in extras:
      for jetindex in ["ij1", "ij2"]:
        toSave = getattr(ev, "Jet_"+var)[jetVars[jetindex]]
        histos[var + jetindex + "_den"].Fill(toSave)
        if passHLT: histos[var + jetindex + "_num"].Fill(toSave)

  tf.Close()

outF = ROOT.TFile(outputHistos, "RECREATE")
for h in histos:
  histos[h].Write()
"""
for var in histBins:
  teff = ROOT.TEfficiency(histos[var+"_num"], histos[var+"_den"]) 
  c = ROOT.TCanvas(var + "C", var + "C", 800, 600)
  teff.SetTitle(";%s;%s"%(var,"Efficiency"))
  teff.Draw("AP")
  c.Update()
  gr = teff.GetPaintedGraph()
  gr.SetMinimum(0)
  gr.SetMaximum(1.4)
  c.SaveAs(outputHistos + "/%s.pdf"%(var))
  c.SaveAs(outputHistos + "/%s.png"%(var))

outF.Close()"""
