Usage:

Running a set of files:

python doVBFHistograms.py [OUTPUT ROOT FILE] [INPUT ROOT FILES]

Submitting files to the cluster (will produce several .root files running over all files inside INPUTFOLDER/XYZ/ ):

python submitJobs_VBF.py [INPUT FOLDER] [OUTPUT FOLDER]

Then for plotting:

python plotHistograms.py [INPUT ROOT FILE] [OUTPUT FOLDER]


An example full workflow:

python submitJobs_VBF.py /eos/cms/store/user/cericeci/VBFTrigger/nano/ /eos/user/c/cericeci/www/VBF/

(wait for jobs to finish)

cd /eos/user/c/cericeci/www/VBF/

hadd output_total.root output*root

cd - 

python plotHistograms.py /eos/user/c/cericeci/www/VBF/output.root /eos/user/c/cericeci/www/VBF/

