#!/usr/bin/env python
import os, re
import commands
import math, time
import sys

print 
print 'START'
print 
########   YOU ONLY NEED TO FILL THE AREA BELOW   #########
########   customization  area #########
interval = 1 # number files to be processed in a single job, take care to split your file so that you run on all files. The last job might be with smaller number of files (the ones that remain).
queue           = "longlunch" 
tag             = "test"
NumberOfJobs    = -1
interval        = 50
doSubmit = True
mainfolder = sys.argv[1] #"/eos/cms/store/user/cericeci/VBFTrigger/nano/"
output = sys.argv[2] #"/eos/user/c/cericeci/www/VBF_3/"
files = []
for era in os.listdir(mainfolder):
  for fil in os.listdir(mainfolder + "/" + era):
    if "root" in fil:
      files.append(mainfolder + "/" + era + "/" + fil)

########   customization end   #########
tag = tag
path = os.getcwd()
print
print 'do not worry about folder creation:'
os.system("rm -rf tmp%s"%tag)
os.system("rm -rf exec%s"%tag)
os.system("rm -rf batchlogs%s"%tag)
os.system("mkdir tmp%s"%tag)
os.system("mkdir exec%s"%tag)
print

if NumberOfJobs == -1: 
  NumberOfJobs = (len(files)+ interval)/interval
##### loop for creating and sending jobs #####
for x in range(1, int(NumberOfJobs)+1):
    ##### creates directory and file list for job #######
    jobFiles = files[max(0,(x-1)*interval):min(x*interval, len(files))]
    with open('exec%s/job_'%tag+str(x)+'.sh', 'w') as fout:
        fout.write("#!/bin/sh\n")
        fout.write("echo\n")
        fout.write("echo\n")
        fout.write("echo 'START---------------'\n")
        fout.write("echo 'WORKDIR ' ${PWD}\n")
        fout.write("export HOME=$PWD\n")
        fout.write("source /afs/cern.ch/cms/cmsset_default.sh\n")
        fout.write("cd /cvmfs/cms.cern.ch/slc7_amd64_gcc700/cms/cmssw/CMSSW_10_6_26/src/ \n")
        fout.write("cmsenv\n")
        fout.write("cd -\n")
        fout.write("python %s/doVBFHistograms.py %s %s\n"%(os.getcwd(), output + "/output_%i.root"%(x), " ".join(jobFiles)))
        fout.write("echo 'STOP---------------'\n")
        fout.write("echo\n")
        fout.write("echo\n")
    os.system("chmod 755 exec%s/job_"%tag+str(x)+".sh")
    
###### create submit.sub file ####
    
os.mkdir("batchlogs%s"%tag)
with open('submit.sub', 'w') as fout:
    fout.write("executable              = $(filename)\n")
    fout.write("arguments               = $(Proxy_path) $(ClusterId)$(ProcId)\n")
    fout.write("output                  = batchlogs%s/$(ClusterId).$(ProcId).out\n"%tag)
    fout.write("error                   = batchlogs%s/$(ClusterId).$(ProcId).err\n"%tag)
    fout.write("log                     = batchlogs%s/$(ClusterId).log\n"%tag)
    fout.write('+JobFlavour = "%s"\n' %(queue))
    fout.write("\n")
    fout.write("queue filename matching (exec%s/job_*sh)\n"%tag)
    
###### sends bjobs ######
os.system("echo submit.sub")
if doSubmit: os.system("condor_submit -spool submit.sub")
  
print
print "your jobs:"
os.system("condor_q")
print
print 'END'
print
